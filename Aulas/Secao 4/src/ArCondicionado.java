public class ArCondicionado {
    private int temperatura;

    public int getTemperatura(){
        return this.temperatura;
    }

    public void trocarTemp(int temperatura){
        if(temperatura >= 17 && temperatura <= 25){
            this.temperatura = temperatura;
        }
    }
}
