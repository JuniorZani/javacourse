package Desafios;

public class ContaPagar {
    private String descricao;
    private double valor;
    private String dataVencimento;

    Fornecedor fornecedor;

    public ContaPagar(){

    }

    public ContaPagar(Fornecedor fornecedor, String descricao, double valor, String dataVencimento){
        this.fornecedor = (fornecedor);
        this.setDescricao(descricao);
        this.setValor(valor);
        this.setDataVencimento(dataVencimento);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }


    public void pagar(){
        System.out.println("\n\nPagamento");
        System.out.println("--------------------------");
        System.out.println("Descrição da conta: " + this.getDescricao());
        System.out.println("Valor: " + this.getValor());
        System.out.println("Data de Vencimento: " + this.getDataVencimento());
        System.out.println("Nome do fornecedor: " + this.fornecedor.getNome());
    }
}
