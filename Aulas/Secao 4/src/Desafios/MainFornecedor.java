package Desafios;

public class MainFornecedor {
    public static void main (String[] args){
        Fornecedor imobiliária = new Fornecedor();
        imobiliária.setNome("Casa & Cia de Negócios Imobiliários");

        Fornecedor mercado =  new Fornecedor();
        mercado.setNome("MErcado do João");

        ContaPagar c1 = new ContaPagar();
        c1.setDescricao("Aluguel da matriz");
        c1.setValor(1230d);
        c1.setDataVencimento("10/05/2012");
        c1.setFornecedor(imobiliária);

        ContaPagar c2 = new ContaPagar(mercado, "Compras do mês", 390d, "19/05/2012");
        ContaPagar c3 = new ContaPagar(mercado, "Aluguel da filial", 700d, "11/05/2012");

        c1.pagar();
        c2.pagar();
        c3.pagar();
    }
}
