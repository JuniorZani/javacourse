package Exercícios;
import java.util.Scanner;

public class Master {

    public static void main( String[] args ){
        Scanner scan = new Scanner(System.in);
        Estoque estoque = new Estoque();
        int quantidade, produtosTotais = 0;

        System.out.print("Quantidade de produtos a serem cadastrados: ");
        quantidade = scan.nextInt();

        estoque.produtos = new Produto[quantidade];

        for(int i = 0; i < quantidade; i++){
            estoque.produtos[i] = new Produto();    //Inicializar a posição do vetor como Produto
            scan.nextLine();                        //Limpar o buffer de scan
            System.out.println("Produto " + (i + 1));
            System.out.println("------------------------------------");
            System.out.print("Nome: ");
            estoque.produtos[i].nome = scan.nextLine();
            System.out.print("Quantidade: ");
            produtosTotais += estoque.produtos[i].quantidade = scan.nextInt();
            System.out.println("Pressione enter para continuar...\n");
            scan.nextLine();
        }
        estoque.listarProdutos();
        System.out.println("\nQuantidade de produtos totais no estoque: " + produtosTotais);
    }
}
