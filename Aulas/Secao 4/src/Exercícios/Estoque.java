package Exercícios;

public class Estoque {
    Produto[] produtos;

    void listarProdutos(){
        System.out.println("Listando os produtos do estoque:");
        System.out.println("--------------------------------\n");
        for(int i = 0; i < this.produtos.length; i++){
            produtos[i].descrever();
        }
        System.out.println("\n--------------------------------");
    }
}
