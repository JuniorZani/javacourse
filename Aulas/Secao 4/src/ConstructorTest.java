public class ConstructorTest {
    public static void main(String[] args){
        Pessoa p1 = new Pessoa("Pedro", 20);
        Pessoa p2 = new Pessoa("Jorge");

        System.out.println("Nome: " + p1.nome + "\nIdade: " + p1.idade);
    }
}
