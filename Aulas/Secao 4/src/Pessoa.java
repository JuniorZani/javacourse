public class Pessoa {
    String nome;
    int idade;

    //Polimorfismo de Sobrecarga
    Pessoa(String nome){
        this.nome = nome;
    }
    Pessoa(String nome, int idade){
        this(nome);
        this.idade = idade;
    }
}
