package aulas.enums.cartas;

public class Carta {
    private int numero;
    private Naipe naipe;

    public Carta(int numero, Naipe naipe){
        this.numero = numero;
        this.naipe = naipe;
    }

    public void exibeCarta(){
        System.out.println(this.numero + " de " + this.naipe + ", sendo o naipe da cor " + this.naipe.getCor());
    }
}
