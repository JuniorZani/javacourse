package aulas.enums.OpAritmetica;

public class Main {

    public static void main(String[] args){
        OpAritimetica op1 = OpAritimetica.ADICAO;
        OpAritimetica op2 = OpAritimetica.SUBTRACAO;

        int soma = op1.operacao(5, 4);
        int sub = op2.operacao(5, 4);

        System.out.println("Adição: " + soma + "\nSubtração: " + sub);

        for(OpAritimetica oa : OpAritimetica.values()){
            System.out.println(oa + " -> " + oa.operacao(4,2));
        }
    }
}
