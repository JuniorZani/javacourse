package aulas.enums.Desafio;

public class ContaPagar{

    private String descricao;
    private double valor;
    private String dataVencimento;
    Fornecedor fornecedor;
    private SituacaoConta situacaoConta;


    ContaPagar(){
        this.situacaoConta = SituacaoConta.PENDENTE;
    }

    public ContaPagar(Fornecedor fornecedor, String descricao, double valor, String dataVencimento){
        this();
        this.fornecedor = (fornecedor);
        this.setDescricao(descricao);
        this.setValor(valor);
        this.setDataVencimento(dataVencimento);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public SituacaoConta getSituacaoConta() {
        return situacaoConta;
    }

    public void pagar(){
        if(this.getSituacaoConta() != SituacaoConta.PENDENTE){
            System.out.println("\nNão é possível realizar o pagamento");
        } else{
            this.situacaoConta = SituacaoConta.PAGA;
            System.out.println("\nPagamento efetuado com sucesso!");
        }
    }

    public void cancelar(){
        if(this.getSituacaoConta() == SituacaoConta.PAGA){
            System.out.println("\nNão é possível cancelar a conta, esta já foi paga");
        } else {
            this.situacaoConta = SituacaoConta.CANCELADA;
            System.out.println("\nCancelamento realizado com sucesso!");
        }
    }
}