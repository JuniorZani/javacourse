package aulas.interfaces.impressora;

public interface Impressora {

    void imprimir(Imprimivel imprimivel);

}
