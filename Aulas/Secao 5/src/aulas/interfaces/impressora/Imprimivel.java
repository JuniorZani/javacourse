package aulas.interfaces.impressora;

public interface Imprimivel {

    String getCabecalhoPagina();
    String getCorpoPagina();

}
