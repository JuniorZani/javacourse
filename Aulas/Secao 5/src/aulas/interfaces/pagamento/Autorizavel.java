package aulas.interfaces.pagamento;

public interface Autorizavel {

    double getValorTotal();
}
