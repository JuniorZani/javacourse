package aulas.interfaces.pagamento;

public class Visa implements Operadora{
    @Override
    public boolean autorizar(Autorizavel autorizavel, Cartao cartao) {
        return autorizavel.getValorTotal() < 500
                && cartao.getNomeTitular().equals("Ademar");
    }
}
