package aulas.interfaces.pagamento;

import aulas.interfaces.pagamento.Autorizavel;
import aulas.interfaces.pagamento.Cartao;
import aulas.interfaces.pagamento.Operadora;

public class Cielo implements Operadora {
    @Override
    public boolean autorizar(Autorizavel autorizavel, Cartao cartao) {
        return autorizavel.getValorTotal() < 100;
    }
}
