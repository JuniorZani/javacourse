package aulas.interfaces.teste;

import aulas.interfaces.impressora.ImpressoraHP;
import aulas.interfaces.pagamento.*;
import aulas.interfaces.caixa.Checkout;
import aulas.interfaces.caixa.Compra;
import aulas.interfaces.impressora.Impressora;
import aulas.interfaces.impressora.ImpressoraEpson;

public class TesteCheckout {
    public static void main(String[] args) {
        Operadora operadora = new Cielo();
        Impressora impressora = new ImpressoraEpson();

        Cartao cartao = new Cartao();
        cartao.setNomeTitular("Jonas");
        cartao.setNumeroCartao(579);

        Compra compra = new Compra();
        compra.setProduto("Lanterna");
        compra.setValortotal(20.50);
        compra.setCliente("Jonas Guilherme");

        Operadora operadoraV = new Visa();
        Impressora impressoraH = new ImpressoraHP();

        Cartao cartaoV = new Cartao();
        cartaoV.setNomeTitular("Ademar");
        cartaoV.setNumeroCartao(579);

        Compra compraV = new Compra();
        compraV.setProduto("Abajour");
        compraV.setValortotal(499.50);
        compraV.setCliente("Ademar Alberto");

        Checkout checkout = new Checkout(operadora, impressora);
        checkout.fecharCompra(compra, cartao);
        System.out.println("\n\n\n");
        Checkout checkoutV = new Checkout(operadoraV, impressoraH);
        checkoutV.fecharCompra(compraV, cartaoV);


    }
}
