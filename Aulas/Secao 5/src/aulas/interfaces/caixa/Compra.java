package aulas.interfaces.caixa;

import aulas.interfaces.impressora.Imprimivel;
import aulas.interfaces.pagamento.Autorizavel;

public class Compra implements Autorizavel, Imprimivel {

    private double valortotal;
    private String cabecalhoPagina;
    private String corpoPagina;
    private String produto;
    private String cliente;

    @Override
    public double getValorTotal() {
        return valortotal;
    }

    @Override
    public String getCabecalhoPagina() {
        return this.getProduto() + " = " + this.getValorTotal();
    }

    @Override
    public String getCorpoPagina() {
        return this.cliente;
    }

    public void setValortotal(double valortotal) {
        this.valortotal = valortotal;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
}
