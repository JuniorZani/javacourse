package aulas.interfaces.caixa;

import aulas.interfaces.pagamento.Cartao;
import aulas.interfaces.pagamento.Operadora;
import aulas.interfaces.impressora.Impressora;

public class Checkout {
    private Operadora operadora;
    private Impressora impressora;

    public Checkout(Operadora operadora, Impressora impressora){
        this.operadora = operadora;
        this.impressora = impressora;
    }

    public void fecharCompra(Compra compra, Cartao cartao){
        boolean autorizado  = this.operadora.autorizar(compra, cartao);

        if(autorizado){
            this.impressora.imprimir(compra);
        } else
            System.out.println("\nPagamento recusado");
    }
}
