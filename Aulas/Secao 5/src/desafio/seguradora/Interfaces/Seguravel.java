package desafio.seguradora.Interfaces;

public interface Seguravel {

    double calcularValorApolice();

    String obterDescricao();

}
