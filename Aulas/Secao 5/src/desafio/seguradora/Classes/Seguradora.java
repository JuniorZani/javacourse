package desafio.seguradora.Classes;

import desafio.seguradora.Interfaces.Seguravel;

public class Seguradora {
    public void fazerPropostaSeguro(Seguravel objetoSeguravel){
        System.out.println("\n---------------------------------");
        System.out.println("Corretora de seguros - Proposta");
        System.out.println(objetoSeguravel.obterDescricao());
        System.out.println("Valor da Apólice: R$" + objetoSeguravel.calcularValorApolice());
        System.out.println("---------------------------------\n");

    }
}
