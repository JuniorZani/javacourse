package desafio.seguradora.Classes;

import desafio.seguradora.Interfaces.Seguravel;

public class Imovel implements Seguravel {
    private double valorMercado;
    private int areaConstruida;

    public Imovel(double valorMercado, int areaConstruida){
        this.valorMercado = valorMercado;
        this.areaConstruida = areaConstruida;
    }

    public double getValorMercado() {
        return valorMercado;
    }

    public void setValorMercado(double valorMercado) {
        this.valorMercado = valorMercado;
    }

    public int getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(int areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Override
    public double calcularValorApolice() {
        double valorApolice = this.valorMercado * 0.003;
        valorApolice += ( this.areaConstruida + 0.5 );
        return valorApolice;
    }

    @Override
    public String obterDescricao() {
        return "Imovel com área construída de " + this.areaConstruida +
                "m² e valor de mercado de R$ " + this.valorMercado;
    }
}
