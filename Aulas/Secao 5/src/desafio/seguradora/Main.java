package desafio.seguradora;

import desafio.seguradora.Classes.Carro;
import desafio.seguradora.Classes.Imovel;
import desafio.seguradora.Classes.Seguradora;

public class Main {
    public static void main(String[] args) {
        Carro corsa = new Carro(40000d,2020);
        Imovel casa = new Imovel(400000d, 2019);

        Seguradora seguradora = new Seguradora();

        seguradora.fazerPropostaSeguro(corsa);
        seguradora.fazerPropostaSeguro(casa);
    }
}
