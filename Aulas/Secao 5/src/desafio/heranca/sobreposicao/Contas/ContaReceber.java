package desafio.heranca.sobreposicao.Contas;

import desafio.heranca.sobreposicao.Pessoas.Cliente;

public class ContaReceber extends Conta{
    private Cliente cliente;

    public ContaReceber(){

    }

    public ContaReceber(Cliente cliente, String descricao, double valor, String dataVencimento){
        this.cliente = cliente;
        this.descricao = descricao;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void exibirDetalhes() {
        super.exibirDetalhes();
        System.out.println("Cliente: " + this.cliente.getNome());
    }

    public void receber() {
        if(this.getSituacaoConta() != SituacaoConta.PENDENTE){
            System.out.println("\nNão é possível receber o pagamento");
        } else{
            this.situacaoConta = SituacaoConta.PAGA;
            System.out.println("\nPagamento recebido com sucesso!");
        }
    }
}
