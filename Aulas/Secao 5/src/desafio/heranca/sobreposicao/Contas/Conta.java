package desafio.heranca.sobreposicao.Contas;

public abstract class Conta {

    protected String descricao;
    protected double valor;
    protected String dataVencimento;
    protected SituacaoConta situacaoConta;
    public Conta(){
        this.situacaoConta = SituacaoConta.PENDENTE;
    }

    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }
    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public SituacaoConta getSituacaoConta() {
        return situacaoConta;
    }

    public void exibirDetalhes(){
        System.out.println("Descrição: " + this.descricao);
        System.out.println("Data de Vencimento: " + this.dataVencimento);
        System.out.println("Situação da conta: " + this.situacaoConta);
        System.out.println("Valor: " + this.valor);
    }
    public void cancelar(){
        if(this.getSituacaoConta() == SituacaoConta.PAGA){
            System.out.println("\nNão é possível cancelar a conta, esta já foi paga");
        } else {
            this.situacaoConta = SituacaoConta.CANCELADA;
            System.out.println("\nCancelamento realizado com sucesso!");
        }
    }
}
