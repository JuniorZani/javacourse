package desafio.heranca.sobreposicao.Contas;

public class RelatorioContas {
    public void exibirListagem(Conta[] conta){
        System.out.print("Exibindo o relatório de todas as contas:");
        for( int i = 0; i < conta.length; i++ ){
            System.out.println("\n\nConta " + (i + 1));
            if( conta[i] instanceof ContaPagar)
                ( (ContaPagar) conta[i] ).exibirDetalhes();
            else
                ( (ContaReceber) conta[i] ).exibirDetalhes();
        }
    }
}
