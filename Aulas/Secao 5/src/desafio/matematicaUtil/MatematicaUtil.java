package desafio.matematicaUtil;

public class MatematicaUtil {

    public static final double PI = 3.14;

    public static int calcularFibonacci(int posicao) {
        if (posicao < 2) {
            return posicao;
        }
        return calcularFibonacci(posicao - 1) + calcularFibonacci(posicao - 2);
    }
    public static double calcularAreaCirculo(double raio){
        if(raio >= 0){
            return MatematicaUtil.PI * raio * raio;
        }
        System.out.println("Valor de raio inválido");
        return 0;
    }
}