public class Main {
    public static void main(String[] args) {
        Carro carro; //Declaração da variável do tipo Carro
        carro = new Carro(); //Inicialização da varíavel "carro"

        carro.modelo = "Corsa";
        carro.fabricante = "Chevrolet";
        carro.anoDeFabricacao = 2006;
        carro.cor = "Amarelo";
        System.out.println("Modelo: " + carro.modelo);
        System.out.println("Ano: " + carro.anoDeFabricacao);
        System.out.println("Fabricante: " + carro.fabricante);
        System.out.println("Cor: " + carro.cor);
        System.out.println();
        carro.ligar();
    }
}