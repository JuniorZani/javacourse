package Desafios;

public class Passeio {
    public static void main(String[] args){
        Pessoa pessoa = new Pessoa();
        Cachorro dodge =  new Cachorro();
        Caminhada caminhada = new Caminhada();
        dodge.nome = "Roger";
        dodge.idade = 5;
        dodge.raca = "Buldog";
        dodge.sexo = 'M';

        pessoa.nome = "Alisson";
        pessoa.dog = dodge;
        caminhada.andar(pessoa);
    }
}
