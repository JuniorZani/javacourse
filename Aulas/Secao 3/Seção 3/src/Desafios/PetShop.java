package Desafios;

public class PetShop {
    public static void main(String[] args){
        Cachorro dodge;
        dodge = new Cachorro();

        dodge.nome = "Roger";
        dodge.idade = 5;
        dodge.raca = "Buldog";
        dodge.sexo = 'M';

        System.out.println("Informações do dog:\nNome: " + dodge.nome +
                "\nIdade: " + dodge.idade + "\nRaça: " + dodge.raca +
                "\nSexo: " + (dodge.sexo == 'M' ? "Macho" : "Fêmea"));
    }
}
