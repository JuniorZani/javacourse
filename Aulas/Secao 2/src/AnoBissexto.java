import java.util.Scanner;
public class AnoBissexto {
    public static void main(String[] args){
        int year;
        Scanner scanf = new Scanner(System.in);

        do {
            System.out.print("Digite o ano: ");
            year = scanf.nextInt();
            if(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
                System.out.println(year + " é um ano bissexto");
            } else
                System.out.println(year + " não é um ano bissexto");
        } while(year != -1);
    }
}