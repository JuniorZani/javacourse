import java.util.Scanner;

public class GuardarProduto {
    public static void main (String[] args){
        Scanner entry = new Scanner(System.in);
        int codigo = 0;
        String resultado = "";

        do{
            System.out.print("Digite o código do produto: ");
            codigo = entry.nextInt();
            if(codigo != 0){
                resultado = codigo % 2 == 0? "Corredor 1, " : "Corredor 2, ";
                for(int i = 8; i > 0; i--){
                    if(codigo % i == 0){
                        resultado += "gaveta " + i;
                        break;
                    }
                }
            }
            resultado = "";
            System.out.println(resultado);
        }while(codigo != 0);
    }
}