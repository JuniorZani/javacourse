import java.util.Scanner;

public class Multa {
    public static void main(String[] args){
        Scanner entry = new Scanner(System.in);

        System.out.print("Digite o tipo do veículo (passeio ou caminhao): ");
        String tipo = entry.nextLine();

        System.out.print("Digite a velocidade permitida: ");
        float velocPermitida = entry.nextFloat();

        System.out.print("Digite a velocidade registrada: ");
        float velocRegistrada = entry.nextFloat();


        if(tipo.equals("passeio") && velocRegistrada >= velocPermitida*1.1){
            System.out.println("Veículo de passeio, multado");
        } else if(tipo.equals("caminhao") && velocRegistrada >= velocPermitida*1.05){
            System.out.println("Caminhão, multado");
        } else {
            System.out.println("Tipo de veículo inválido");
        }
    }
}
