public class Calculadora {
    public static void main(String[] args){
        float pi, raio, area;
        pi = (float) 3.14;
        raio = 2;

        area = pi*(raio*raio);

        System.out.println("Área em float: " + area);
        System.out.println("Área em int: " + (int)area);

    }
}