import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        DateFormat formatador = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        System.out.println(formatador.format(new Date()));


        Calendar c = new GregorianCalendar();
        c.set(Calendar.DAY_OF_MONTH, 15);
        c.set(Calendar.MONTH, 8);
        c.set(Calendar.YEAR, 2010);
        c.set(Calendar.HOUR_OF_DAY, 10);
        c.set(Calendar.MINUTE, 30);
        c.set(Calendar.SECOND, 20);
        System.out.println(formatador.format(c.getTime()));
    }
}
