package aulas.ordenacao;

import java.util.Comparator;

public class idadeComparator implements Comparator<Pessoa> {

    @Override
    public int compare(Pessoa p1, Pessoa p2) {
        //Se a pessoa x tiver a idade menor que a pessoa x+1
        if(p1.getIdade() < p2.getIdade()){
            return 1; //P1 irá na "frente" de p2

        //Se a pessoa x tiver a idade maior que a pessoa x+1
        } else if(p1.getIdade() > p2.getIdade()){
            return -1; //troca de lugar
        }
        return 0;
    }
}
