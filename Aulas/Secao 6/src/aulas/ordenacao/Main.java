package aulas.ordenacao;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Pessoa p1 = new Pessoa("João", 21);
        Pessoa p2 = new Pessoa("Maria", 20);
        Pessoa p3 = new Pessoa("Xavier", 22);

        List<Pessoa> pessoas = Arrays.asList(p1,p2,p3);

        idadeComparator idade_comparator = new idadeComparator();

        System.out.println("\nSem ordenação:");
        imprimirPessoas(pessoas);
        Collections.sort(pessoas);//Ordenando pelo nome
        System.out.println("\nOrdenando pelo nome:");
        imprimirPessoas(pessoas);
        System.out.println("\nOrdenando pela idade:");
        Collections.sort(pessoas, idade_comparator);
        imprimirPessoas(pessoas);
    }

    public static void imprimirPessoas(List<Pessoa> pessoas){
        for (Pessoa p : pessoas) {
            System.out.println(p.getNome() + ", idade: " + p.getIdade());
        }
    }
}
