package aulas.excecoes;

public class ContaCorrente {
    private double saldo;

    public ContaCorrente(double saldo){
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void sacar(double valor) throws SaldoInsuficienteException{
        if((this.saldo - valor) < 0 ){
            throw new SaldoInsuficienteException("Saldo insuficiente para saque");
        }
        this.saldo -= valor;
    }

    public void depositar(double valor){
        if(valor <= 0){
            throw new IllegalArgumentException("Valor deve ser >= 0");
        }
        this.saldo += valor;
    }
}
