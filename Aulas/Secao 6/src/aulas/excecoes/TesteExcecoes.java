package aulas.excecoes;

import java.util.Locale;

public class TesteExcecoes {
    public static void main(String[] args) {
//        int numero = 5/0;
//        System.out.println(numero);  /*Arithmetic excp*/

//        String s = "txto";
//        s = null;
//        String a = s.toUpperCase();  /*Null pointer excp.*/
        ContaCorrente cc = new ContaCorrente(200);

        try{
            cc.depositar(-10);
        } catch(IllegalArgumentException e){
            System.out.println("Valor de depósito inválido\n" + e.getMessage());
        }
    }
}
