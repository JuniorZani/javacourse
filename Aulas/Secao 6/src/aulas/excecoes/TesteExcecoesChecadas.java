package aulas.excecoes;

public class TesteExcecoesChecadas {
    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente(100);
        try{
            cc.sacar(60);
            System.out.println("Saldo: " + cc.getSaldo());
        } catch( SaldoInsuficienteException e){
            System.out.println(e.getMessage());
        }

        try{
            cc.sacar(50);
            System.out.println("Saldo: " + cc.getSaldo());
        } catch( SaldoInsuficienteException e){
            System.out.println("ERRO:\n" + e.getMessage());
        }
    }
}
