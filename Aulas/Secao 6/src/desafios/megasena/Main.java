package desafios.megasena;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MegaSena mega = new MegaSena();
        Scanner scan = new Scanner(System.in);
        int quantidade;
        System.out.print("Numero de Jogos: ");
        quantidade = scan.nextInt();

        for(int i = 0; i < quantidade; i++){
            mega.sortear();
            mega.printarResultado();
        }
    }
}
