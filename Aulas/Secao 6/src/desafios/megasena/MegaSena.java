package desafios.megasena;

/*
    Requisitos:
    - Receber quantos jogos serão gerados
    - Gerar "n" jogos com 6 números aleatórios diferentes
*/
public class MegaSena {
    static final int NUMEROS_TOTAIS = 6;
    static final int NUMERO_MAX = 60;

    private int[] numeros;

    public void sortear() {
        this.numeros = new int[NUMEROS_TOTAIS];
        int numero;

        for(int i = 0; i < NUMEROS_TOTAIS; i++){
            numero = (int) Math.ceil(Math.random()*NUMERO_MAX);
            if(!this.repetido(numero)){
                numeros[i] = numero;
            }
        }
    }

    public void printarResultado(){
        System.out.println("Exibindo resultados: ");
        for(int i = 0; i < NUMEROS_TOTAIS; i++){
            System.out.print(this.numeros[i] + " ");
        }
        System.out.println();
    }
    private boolean repetido(int numero) {
        for (int j : numeros) {
            if (numero == j)
                return true;
        }
        return false;
    }
}