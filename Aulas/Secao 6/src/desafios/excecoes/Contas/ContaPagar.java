package desafios.excecoes.Contas;

import desafios.excecoes.Pessoas.Fornecedor;

public class ContaPagar extends Conta{

    private Fornecedor fornecedor;
    public ContaPagar(){
        this.situacaoConta = SituacaoConta.PENDENTE;
    }

    public ContaPagar(Fornecedor fornecedor, String descricao, double valor, String dataVencimento){
        this();
        this.fornecedor = (fornecedor);
        this.setDescricao(descricao);
        this.setValor(valor);
        this.setDataVencimento(dataVencimento);
    }
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    @Override
    public void exibirDetalhes() {
        super.exibirDetalhes();
        System.out.println("Fornecedor: " + this.fornecedor.getNome());
    }

    public void pagar() throws OperacaoContaException{
        if(this.getSituacaoConta() != SituacaoConta.PENDENTE){
            throw new OperacaoContaException("Conta não está pendente");
        } else{
            this.situacaoConta = SituacaoConta.PAGA;
            System.out.println("Pagamento efetuado com sucesso!");
        }
    }
}