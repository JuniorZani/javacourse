package desafios.excecoes.Contas;

import desafios.excecoes.Contas.ContaReceber;
import desafios.excecoes.Pessoas.Cliente;

public class ContaReceber extends Conta{
    private Cliente cliente;

    public ContaReceber(){

    }

    public ContaReceber(Cliente cliente, String descricao, double valor, String dataVencimento){
        this.cliente = cliente;
        this.descricao = descricao;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void exibirDetalhes() {
        super.exibirDetalhes();
        System.out.println("Cliente: " + this.cliente.getNome());
    }

    public void receber() throws OperacaoContaException{
        if(this.getSituacaoConta() != SituacaoConta.PENDENTE){
            throw new OperacaoContaException("Não é possível receber o pagamento");
        } else{
            this.situacaoConta = SituacaoConta.PAGA;
            System.out.println("Pagamento recebido com sucesso!");
        }
    }
}
