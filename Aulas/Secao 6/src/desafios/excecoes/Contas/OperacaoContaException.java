package desafios.excecoes.Contas;

public class OperacaoContaException extends Exception{

    public OperacaoContaException(String message){
        super(message);
    }
}
