package desafios.numeros;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // Recebe como parâmetro do usuário o valor de um produto do tipo double.
        // Instancia um objeto do tipo BigDecimal atribuindo o valor double a ele.

        System.out.print("Digite o preço do produto: ");
        BigDecimal valorProduto = BigDecimal.valueOf(scan.nextDouble());
        // Calcula 10% do valor usando os métodos disponíveis na classe BigDecimal.
        System.out.println("Valor inserido: " + valorProduto);
        valorProduto =  valorProduto.multiply(BigDecimal.valueOf(0.1));

        // Formata o novo valor calculado para o padrão monetário (R$).
        DecimalFormat df = new DecimalFormat("R$ ##,##0.00");
        // Exibe na tela o valor calculado e formatado.
        System.out.println("10% do valor: " + valorProduto + "\n10% formatado: " + df.format(valorProduto));
    }
}
