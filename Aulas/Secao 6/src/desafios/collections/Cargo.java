package desafios.collections;

import java.math.BigDecimal;

/***
 * Classe que representa os Cargos dos objetos tipo Político
 * cada Cargo contém uma descriçõa e o valor salarial
 */
public class Cargo {

    private String descricao;
    private BigDecimal salario;

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getSalario() {
        return this.salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

}